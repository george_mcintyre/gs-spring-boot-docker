# README #

This is a training project for creating spring boot apps.  It is to be used by the leveln team only

### What is this repository for? ###

* Spring Boot, Docker
* Version 1.0

### How do I get set up? ###

* Download sources
* Configuration
* Requires Java 1.8+, Maven, Docker
* To build: mvn install
* To build docker image: mvn dockerfile:build
* To push image to repo: mvn dockerfile:push
* To run: docker run -p 8080:8080 -t leveln/gs-spring-boot-docker
* To test: go to http://localhost:8080/

### Who do I talk to? ###

* george@level-n.com owner
