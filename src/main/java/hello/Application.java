/**
 * User: george
 * Date: 28/08/2017
 * Time: 17:34
 */
package hello;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@SpringBootApplication
@RestController
public class Application {

    @RequestMapping("/")
    public String home() {
        return "Hello New Docker World";
    }

    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }

}